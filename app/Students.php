<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Classes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Students
 *
 * @mixin \Eloquent
 * @property int $student_id
 * @property int $class_id
 * @property string $last_name
 * @property string $first_name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Classes $studentClass
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Students whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Students whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Students whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Students whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Students whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Students whereUpdatedAt($value)
 */
class Students extends Model
{
    const TABLE_NAME = 'students';
    const PRIMARY_KEY = 'student_id';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::PRIMARY_KEY;

    const VALIDATION_RULES_CREATE = [
        'class_id' => 'required|exists:' . Classes::TABLE_NAME . ',' . Classes::PRIMARY_KEY ,
        'last_name' => 'required|max:255',
        'first_name' => 'required|max:255',
    ];

    const VALIDATION_RULES_UPDATE = [
        'class_id' => 'required|exists:' . Classes::TABLE_NAME . ',' . Classes::PRIMARY_KEY ,
        'last_name' => 'required|max:255',
        'first_name' => 'required|max:255',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function studentClass(): BelongsTo
    {
        return $this->belongsTo(Classes::class,Classes::PRIMARY_KEY, Classes::PRIMARY_KEY);
    }
}
