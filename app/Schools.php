<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Schools
 *
 * @mixin \Eloquent
 * @property int $school_id
 * @property string $school_name
 * @property string $city
 * @property string $address
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Classes[] $classes
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schools whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schools whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schools whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schools whereSchoolId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schools whereSchoolName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schools whereUpdatedAt($value)
 */
class Schools extends Model
{
    const TABLE_NAME = 'schools';
    const PRIMARY_KEY = 'school_id';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::PRIMARY_KEY;

    const VALIDATION_RULES_CREATE = [
        'school_name' => 'required|string|max:255',
        'city' => 'required|string|max:255',
        'address' => 'required|string|max:255',
    ];

    const VALIDATION_RULES_UPDATE = [
        'school_name' => 'required|string|max:255',
        'city' => 'required|string|max:255',
        'address' => 'required|string|max:255',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function classes(): HasMany
    {
        return $this->hasMany(Classes::class, self::PRIMARY_KEY, self::PRIMARY_KEY);
    }
}
