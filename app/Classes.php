<?php

namespace App;

use Felixkiss;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Classes
 *
 * @mixin \Eloquent
 * @property int $class_id
 * @property int $school_id
 * @property int $year
 * @property int $ordinal
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Schools $school
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classes whereClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classes whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classes whereOrdinal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classes whereSchoolId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classes whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Classes whereYear($value)
 */
class Classes extends Model
{
    const TABLE_NAME = 'classes';
    const PRIMARY_KEY = 'class_id';

    protected $table = self::TABLE_NAME;
    protected $primaryKey = self::PRIMARY_KEY;

    const VALIDATION_RULES_CREATE = [
        'school_id' => 'required|exists:' . Schools::TABLE_NAME . ',' . Schools::PRIMARY_KEY ,
        'year' => 'required|integer|min:1|max:8|unique_with:'. self::TABLE_NAME . ',ordinal,school_id',
        'ordinal' => 'required,|integer,|min:1,|max:8',
    ];

    const VALIDATION_RULES_UPDATE = [
        'school_id' => 'required|exists:' . Schools::TABLE_NAME . ',' . Schools::PRIMARY_KEY ,
        'year' => [
            'required',
            'integer',
            'min:1',
            'max:8',
        ],
        'ordinal' => 'required|integer|min:1|max:8',
    ];

    /**
     * School relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function school(): BelongsTo
    {
        return $this->belongsTo( Schools::class, Schools::PRIMARY_KEY, \App\Schools::PRIMARY_KEY);
    }

    /**
     * Students relation
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function students(): HasMany
    {
        return $this->hasMany(Students::class, self::PRIMARY_KEY, self::PRIMARY_KEY);
    }

}
