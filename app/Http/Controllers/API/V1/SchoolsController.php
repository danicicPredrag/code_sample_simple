<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Schools;

class SchoolsController extends Controller
{
    /**
     * Get list of Schools
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $schools = Schools::paginate(config('constants.pagination.schools_per_page'));

        return response()->json($schools, 200);
    }

    /**
     * Get Single School
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {

        $school = Schools::with('classes')->find($id);

        if (empty($school)) {
            return response()->json(['message' => trans('messages.errors.not_found_school')], 400);
        }

        return response()->json($school, 200);

    }

    /**
     * Create new School
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), Schools::VALIDATION_RULES_CREATE);

        if ($validator->fails()) {
           return response()->json($validator->errors(), 400);
        }

        $school = new Schools;
        $school->school_name = $request->input('school_name');
        $school->city = $request->input('city');
        $school->address = $request->input('address');
        if (!$school->save()) {
            return response()->json(['message' => trans('messages.errors.saving_school')], 400);
        }
        return response()->json(['message' => trans('messages.messages.added_new_school')], 200);

    }

    /**
     * Update existing School
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), Schools::VALIDATION_RULES_CREATE);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $school = Schools::find($id);
        if (empty($school)) {
            return response()->json(['message' => trans('messages.errors.not_found_school')], 400);
        }

        $school->school_name = $request->input('school_name');
        $school->city = $request->input('city');
        $school->address = $request->input('address');

        if (!$school->save()) {
            return response()->json(['message' => trans('messages.errors.updating_school')], 400);
        }

        return response()->json(['message' => trans('messages.messages.updated_school')], 200);
    }

    /**
     * Delete School
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $school = Schools::with('classes')->find($id);
        if (!empty($school->classes)) {
            return response()->json(['message' => trans('messages.errors.school_have_classes')], 400);
        }

        $school->delete();

        return response()->json(['message' => trans('messages.messages.deleted_school')], 200);
    }

}

