<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\Students;

class StudentsController extends Controller
{

    /**
     * Get list of students
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $students = Students::paginate(config('constants.pagination.students_per_page'));

        return response()->json($students, 200);
    }


    /**
     * Get single student
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {

        $student = Students::with(['studentClass' => function ($query) {
            $query->with('school');
        }])->find($id);

        if (empty($student)) {
            return response()->json(['message' => trans('messages.errors.not_found_student')], 400);
        }

        return response()->json($student, 200);

    }


    /**
     * Create new student
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), Students::VALIDATION_RULES_CREATE);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $student = new Students;
        $student->class_id = $request->input('class_id');
        $student->last_name = $request->input('last_name');
        $student->first_name = $request->input('first_name');

        if (!$student->save()) {
            return response()->json(['message' => trans('messages.errors.saving_student')], 400);
        }
        return response()->json(['message' => trans('messages.messages.added_new_student')], 200);

    }


    /**
     * Update existing student
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $validator = Validator::make($request->all(), Students::VALIDATION_RULES_UPDATE);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $student = Students::find($id);
        if (empty($student)) {
            return response()->json(['message' => trans('messages.errors.not_found_student')], 400);
        }

        $student->class_id = $request->input('class_id');
        $student->last_name = $request->input('last_name');
        $student->first_name = $request->input('first_name');

        if (!$student->save()) {
            return response()->json(['message' => trans('messages.errors.updating_student')], 400);
        }

        return response()->json(['message' => trans('messages.messages.updated_student')], 200);

    }

    /**
     * Delete Student
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        $student = Students::find($id);
        if (empty($student)) {
            return response()->json(['message' => trans('messages.errors.not_found_student')], 400);
        }

       $student->delete();

        return response()->json(['message' => trans('messages.messages.deleted_student')], 200);

    }
}
