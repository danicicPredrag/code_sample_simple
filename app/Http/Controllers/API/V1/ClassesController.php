<?php

namespace App\Http\Controllers\API\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use Illuminate\Validation\Rule;
use App\Classes;
use App\Students;

class ClassesController extends Controller
{
    /**
     * Get List of Classes
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $classes = Classes::paginate(config('constants.pagination.classes_per_page'));

        return response()->json($classes, 200);

    }

    /**
     * Get single School
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $class = Classes::with(['school', 'students'])->find($id);

        if (empty($class)) {
            return response()->json(['message' => trans('messages.errors.not_found_class')], 400);
        }

        return response()->json($class, 200);

    }

    /**
     * Create new School
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), Classes::VALIDATION_RULES_CREATE);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $class = new Classes;
        $class->school_id = $request->input('school_id');
        $class->year = $request->input('year');
        $class->ordinal = $request->input('ordinal');
        if (!$class->save()) {
            return response()->json(['message' => trans('messages.errors.saving_class')], 400);
        }
        return response()->json(['message' => trans('messages.messages.added_new_class')], 200);
    }

    /**
     * Update existing School
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $validator_rules = Classes::VALIDATION_RULES_UPDATE;
        $validator_rules['year'][] = Rule::unique(Classes::TABLE_NAME)->where(function ($query) use ($request, $id){
            return $query
                ->where('year', $request->input('school_id'))
                ->where('ordinal' , $request->input('ordinal'))
                ->where('school_id' , $request->input('school_id'))
                ->where(Classes::PRIMARY_KEY, '!=', $id);
        });


        $validator = Validator::make($request->all(), $validator_rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $class = Classes::find($id);
        if (empty($class)) {
            return response()->json(['message' => trans('messages.errors.not_found_class')], 400);
        }

        $class->school_id = $request->input('school_id');
        $class->year = $request->input('year');
        $class->ordinal = $request->input('ordinal');
        if (!$class->save()) {
            return response()->json(['message' => trans('messages.errors.updating_class')], 400);
        }
        return response()->json(['message' => trans('messages.messages.updated_class')], 200);
    }

    /**
     * Delete Existing School
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        $class = Classes::with('students')->find($id);

        if (empty($class)) {
            return response()->json(['message' => trans('messages.errors.not_found_student')], 400);
        }

        if (!empty($class->students())) {
            Students::where(Classes::PRIMARY_KEY, $id)->delete();
        }

        $class->delete();

        return response()->json(['message' => trans('messages.messages.deleted_class')], 200);
    }

}
