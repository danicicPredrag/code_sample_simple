<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

use App\Classes;
use App\Students;


class StudentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Factory::create();

        $classes = \App\Classes::get();
        foreach ($classes as $class) {
            $students_in_class = rand(20, 30);
            for ($i = 1; $i < $students_in_class; $i++) {
                $student = new Students();
                $student->last_name = $faker->firstName();
                $student->first_name = $faker->lastName();
                $student->class_id = $class->{Classes::PRIMARY_KEY};
                $student->save();
            }
        }
    }
}
